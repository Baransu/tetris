import { TILE_SIZE, SHAPES } from './config'
import Block from './Block'

export default class Tetris {
  constructor(ctx, width, height) {
    this.ctx = ctx;
    this.width = width;
    this.height = height;
    this.lastTime = Date.now();
    this.deltaTime = 0;
    this.blocks = [];
    this.keys = [];
  }

  start() {
    const blocksX = this.width/TILE_SIZE;
    const blocksY = this.height/TILE_SIZE;
    for(let y = 0; y < blocksY; y++) {
      this.blocks[y] = [];
      for(let x = 0; x < blocksX; x++) {
        this.blocks[y][x] = -1;
      }
    }

    this.fallingBlock = this.newFalling();

    this.setListeners()
    this.gameLoop()
  }

  gameLoop() {
    const now = Date.now();
    this.deltaTime = (now - this.lastTime) / 1000;

    this.update()
    this.render()

    this.lastTime = now;
    requestAnimationFrame(this.gameLoop.bind(this))
  }

  newFalling() {
    const blocksX = this.width/TILE_SIZE;
    const index = Math.floor(Math.random() * (SHAPES.length - 1))
    return new Block(~~(blocksX/2) - 2, -2, SHAPES[index], this.onFallen.bind(this))
  }

  onFallen() {
    const blocksX = this.width/TILE_SIZE;

    // add shape to scene
    const { x, y, shape, color } = this.fallingBlock;
    const lenY = shape.length;
    for(let yy = 0; yy < lenY; yy++) {
      const lenX = shape[yy].length;
      for(let xx = 0; xx < lenX; xx++) {
        if(shape[yy][xx] === 1) {
          this.blocks[y + yy][x + xx] = color;
        }
      }
    }

    this.fallingBlock = this.newFalling()
  }

  update() {
    this.fallingBlock.update(this.deltaTime, this.blocks, this.keys)
  }

  render() {
    // clear
    this.ctx.fillStyle = 'white';
    this.ctx.fillRect(0, 0, this.width, this.height);

    //fallen blocks
    this.ctx.save()
    this.ctx.strokeStyle = 'lightgrey';
    const lenY = this.blocks.length;
    for(let y = 0; y < lenY; y++) {
      const lenX = this.blocks[y].length;
      for(let x = 0; x < lenX; x++) {
        const color = this.blocks[y][x];
        if(color !== -1) {
          this.ctx.fillStyle = color;
          this.ctx.fillRect(TILE_SIZE * x, TILE_SIZE * y, TILE_SIZE, TILE_SIZE)
        } else {
          this.ctx.strokeRect(TILE_SIZE * x, TILE_SIZE * y, TILE_SIZE, TILE_SIZE)
        }
      }
    }
    this.ctx.restore();

    // render falling block
    this.fallingBlock.render(this.ctx);
  }

  setListeners() {
    document.body.addEventListener('keydown', this.keydown.bind(this), false);
    document.body.addEventListener('keyup', this.keyup.bind(this), false);
  }

  removeListeners() {
    document.body.removeEventListener('keydown', this.keydown.bind(this), false);
    document.body.removeEventListener('keyup', this.keyup.bind(this), false);
  }

  keydown(e) {
    //console.log(e.which);
    this.keys[e.which] = true;
  }

  keyup(e) {
    this.keys[e.which] = false;
  }
}
