import { rotate } from './Block'

describe('Block class', () => {
  it('should rotate shape', () => {
    expect(rotate([
      [0, 0, 1],
      [1, 1, 1]
    ])).toEqual([
      [1, 0],
      [1, 0],
      [1, 1]
    ])
  })
})
