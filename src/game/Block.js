import { TILE_SIZE } from './config'

// TODO move to class
export const rotate = (old) => {
  let shape = [];
  const lenY = old.length;
  const lenX = old[0].length;
  for(let x = 0; x < lenX; x++) {
    shape[x] = [];
    for(let y = lenY - 1; y >= 0; y--) {
      shape[x][lenY - y - 1] = old[y][x];
    }
  }
  return shape;
}

export const canRotate = (x, y, shape, blocks) => {
  const sceneWidth = blocks[0].length;
  const shapeWidth = shape[0].length;
  const isInside = x + shapeWidth <= sceneWidth;
  return isInside && canMove(x, y, shape, blocks);
}

export const canMove = (x, y, shape, blocks) => {
  const len = blocks.length;
  const lenY = shape.length;

  if(y < 0) return true;
  else if(y >= len || y + lenY > len) return false;
  else {
    let error = false;
    for(let yy = 0; yy < lenY; yy++) {
      const lenX = shape[yy].length;
      for(let xx = 0; xx < lenX; xx++) {
        if(!error) {
          error = shape[yy][xx] === 1 && blocks[y + yy][x + xx] !== -1;
        }
      }
    }
    return !error;
  }
}

export default class Block {
  constructor(x, y, { shape, color }, onFallen) {
    this.color = color;
    this.shape = shape;
    this.x = x;
    this.y = y;
    this.onFallen = onFallen;
    this.times = {
      rotation: 0.15,
      horizontal: 0.05,
      down: 1,
      accelerated: 0.05
    }
    this.timers = {
      rotation: 0,
      horizontal: 0,
      down: this.times.down
    }
    this.hold = {
      rotation: false,
      horizontal: false
    }
  }

  update(deltaTime, blocks, keys) {
    // rotate
    if(!this.hold.rotation && this.timers.rotation <= 0 && keys[38]) {
      // check if can rotate
      const newShape = rotate(this.shape);
      if(canRotate(this.x, this.y, newShape, blocks)) {
        this.shape = newShape;
        this.timers.rotation = this.times.rotation;
      }
      this.hold.rotation = true;
    } else if(!keys[38]){
      this.hold.rotation = false;
    }

    //move
    if(!this.hold.horizontal && this.timers.horizontal <= 0) {
      const len = this.shape[0].length;
      const width = blocks[0].length;
      if( keys[37]) { //left
        if(this.x > 0 && canMove(this.x - 1, this.y, this.shape, blocks)) {
          this.x--;
          this.timers.horizontal = this.times.horizontal;
          this.hold.horizontal = true;
        }
      } else if(keys[39]) { //right
        if(this.x + len < width && canMove(this.x + 1, this.y, this.shape, blocks)) {
          this.x++;
          this.timers.horizontal = this.times.horizontal;
          this.hold.horizontal = true;
        }
      }
    } else if(!keys[37] && !keys[39]) {
      this.hold.horizontal = false;
    }

    if(keys[40] && this.times.down > this.times.accelerated) {
      this.timers.down = this.times.down = this.times.accelerated;
    }

    //move down
    if(this.timers.down <= 0) {
      if(canMove(this.x, this.y + 1, this.shape, blocks)) {
        this.y++;
        this.timers.down = this.times.down;
      } else {
        this.onFallen()
      }
    }

    // decrease all timers
    Object.keys(this.timers).forEach(timer => {
      if(this.timers[timer] > 0)
        this.timers[timer] -= deltaTime;
    })
  }

  render(ctx) {
    ctx.save()
    ctx.fillStyle = this.color;
    ctx.strokeStyle = 'lightgrey';
    const lenY = this.shape.length;
    for(let y = 0; y < lenY; y++) {
      const lenX = this.shape[y].length;
      for(let x = 0; x < lenX; x++) {
        if(this.shape[y][x]) {
          ctx.fillRect(TILE_SIZE * (this.x + x), TILE_SIZE * (this.y + y), TILE_SIZE, TILE_SIZE);
        }
      }
    }
    ctx.restore()
  }
}
