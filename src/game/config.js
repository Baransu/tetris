//export const TILE_SIZE = 60;
export const TILE_SIZE = 30;
export const SHAPES = [
  {
    color: 'orange',
    shape: [
      [0, 0, 1],
      [1, 1, 1]
    ]
  },

  {
    color: 'aqua',
    shape: [
      [1, 1, 1, 1]
    ]
  },

  {
    color: 'blue',
    shape: [
      [1, 0, 0],
      [1, 1, 1]
    ]
  },

  {
    color: 'yellow',
    shape: [
      [1, 1],
      [1, 1]
    ]
  },

  {
    color: 'green',
    shape: [
      [0, 1, 1],
      [1, 1, 0]
    ]
  },

  {
    color: 'purple',
    shape: [
      [0, 1, 0],
      [1, 1, 1]
    ]
  },

  {
    color: 'red',
    shape: [
      [1, 1, 0],
      [0, 1, 1]
    ]
  }
]
