import React, { Component } from 'react'

import Tetris from '../game';

export default class Game extends Component {
  componentDidMount() {
    const ctx = this.canvas.getContext('2d');
    const width = 540;
    const height = 900;
    this.canvas.width = width;
    this.canvas.height = height;
    this.canvas.style.position = 'absolute';

    this.resizeCanvas()
    window.addEventListener('resize', this.resizeCanvas.bind(this), false)

    this.game = new Tetris(ctx, width, height);
    this.game.start();
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.resizeCanvas.bind(this), false)
    this.game.remiveListeners()
  }

  resizeCanvas() {
    const width = this.container.clientWidth;
    const scaleX = width / this.canvas.width;
    const scaleY = window.innerHeight  / this.canvas.height;

    const scaleToFit = Math.min(scaleX, scaleY);

    const w = width - (this.canvas.width * scaleToFit);
    const h = window.innerHeight - (this.canvas.height * scaleToFit);

    this.canvas.style.top = `${h/2}px`;
    this.canvas.style.left = `${w/2}px`;

    this.canvas.style.transformOrigin = "0 0"; //scale from top left
    this.canvas.style.transform = `scale(${scaleToFit})`;
  }

  render() {
    return (
      <div className={this.props.className} ref={div => this.container = div}>
        <canvas ref={canvas => this.canvas = canvas}></canvas>
      </div>
    )
  }
}
