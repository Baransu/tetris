import React, { Component } from 'react';
import './App.css';
import Game from './components/Game';

class App extends Component {
  render() {
    return (
      <div className="app">
        <Game className="game"/>
        <div className="side-panel">tutaj bedzie score/next block canvas i inne bajki</div>
      </div>
    );
  }
}

export default App;
